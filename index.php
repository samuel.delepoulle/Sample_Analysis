<meta http-equiv="content-type" content="text/html; charset=utf-8" /> 

<HTML>
<HEAD>
    <TITLE>CornellBox </TITLE>
	
<BODY>

    <P>
	<div style="width:512px; float:left; background-color:#E6AB81; padding:20px; text-align:justify;"> 
		<?php 

			// chemin des données
			$data_path = '/mnt/graal/cornellBox/distrib/';	
			
			$lesScenes = array(
				"CornellBoxNonVide",
				"CornellBoxNonVideTextureArcade",
				"CornellBoxNonVideTextureDegrade",			
				"CornellBoxNonVideTextureDamier",
				"CornellBoxVide",
				"CornellBoxVideTextureDamier",
				"Bar1",
				"Bureau1_cam2",
				"Boulanger",
				"Exterieur01",
				"Sponza1"				
			);
			$MAX = 1000;

			session_start ();

			//nettoyer le graphique 
			shell_exec("rm fig.png");			
			$adresse = $_SERVER['PHP_SELF'];
			
			if (!empty($_REQUEST['scene'])){
				$_SESSION['scene'] = $_REQUEST['scene'];
			}						
			$scene=$_SESSION['scene'];
			
			//echo $scene;
			
			
			
			if (!empty($_REQUEST['ech'])){
				$n = $_REQUEST['ech'];
				//if ($n<1) $n=1;
				//if ($n>$MAX) $n=$MAX;
				$_SESSION['ech'] = $n;				
			} else if(!isset($_SESSION['ech'])){
				$_SESSION['ech'] = $MAX;
			}
			$ech = $_SESSION['ech'];
			
			echo "<A href=$adresse> <IMG src='images/".$scene."_10.png' ismap border='0'> </A>";
		?>	
		
		<form>
			<select name="scene" onClick="javascript:this.form.submit();">
				<!--<option value=""> ----- Choisir ----- </option>-->
				<?php
					foreach ($lesScenes as $i){
						if ($i == $scene){
							$selected="selected";
						}
						else{
							$selected="";
						}
							
						echo "<option value='$i' $selected>$i</option>";
					}
				?>
				<!--<option value="CornellBoxNonVide"> CornellBoxNonVide </option>
				<option value="CornellBoxNonVideTextureDegrade"> CornellBoxNonVideTextureDegrade </option>	-->
			</select>
			</p>
			échantillons analysés : 
			<input name="ech" type="text" size="5" value="<?php echo $ech?>" onChange="javascript:this.form.submit();">
			<input name="OK" type="submit" value="changer">
		</form>		
		
	</div>	

	<div style="width:400px; float:left; background-color:#E6AB81; padding:20px; text-align:justify;"> 

<?php
	

	if (!empty($_SERVER['QUERY_STRING'])){
		
		if (empty($_REQUEST['scene'])){
		
			//echo $_SERVER['QUERY_STRING'];
					
			$coords = explode (',', $_SERVER['QUERY_STRING']);
			
			$affiche = '(' . $coords[0] . ', ' . $coords[1] . ')';
			echo '<p style="color: red;">Point ' . $affiche . '</p>';
			
			$fichier_dat = $data_path.$scene.'/'.$coords[0].'/'.$scene.'_'.$coords[0].'_'.$coords[1].'.dat';

			//echo $fichier_dat;

			echo "<BR>";
			
			$commande = "Rscript analyse/analyse.R ".$fichier_dat." ".$ech;
			//echo $commande;
			$ligne = shell_exec($commande);
			echo "<IMG src='fig.png'>";
			echo "<pre>$ligne</pre>";
		}
	}
?>

</div>
